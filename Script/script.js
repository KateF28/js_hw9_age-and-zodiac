// ## Задание
// Реализовать функцию, которая будет считать возраст пользователя и его знак зодиака.

const zodiacSigns = ["Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces"];
let birthDate = prompt('Enter your birthday date in the next format: "dd.mm.yyyy"', '22.03.2002');
let nowDate = new Date();
let nowYear = nowDate.getFullYear();
let nowMonth = nowDate.getMonth() + 1;
let nowDayOfMonth = nowDate.getDate();
let year = birthDate.slice(6,10);
let dayOfMonth = parseInt(birthDate.slice(0,2), 10);
let month = parseInt(birthDate.slice(3,5), 10);
const longMonthes = [1, 3, 5, 7, 8, 10, 12];
let isLongMonth = (longMonth) => { return longMonth === month;};
const shortMonthes = [4, 6, 9, 11];
let isShortMonth = (shortMonth) => { return shortMonth === month;};

// Определяем знак зодиака
if ((month===3 && dayOfMonth>=21) || (month===4 && dayOfMonth<=19)) {
	alert(`Your zodiac sign is ${zodiacSigns[0]}`);
} else if ((month===4 && dayOfMonth>=20) || (month===5 && dayOfMonth<=20)) {
	alert(`Your zodiac sign is ${zodiacSigns[1]}`);
} else if ((month===5 && dayOfMonth>=21) || (month===6 && dayOfMonth<=21)) {
	alert(`Your zodiac sign is ${zodiacSigns[2]}`);
} else if ((month===6 && dayOfMonth>=22) || (month===7 && dayOfMonth<=22)) {
	alert(`Your zodiac sign is ${zodiacSigns[3]}`);
} else if ((month===7 && dayOfMonth>=23) || (month===8 && dayOfMonth<=22)) {
	alert(`Your zodiac sign is ${zodiacSigns[4]}`);
} else if ((month===8 && dayOfMonth>=23) || (month===9 && dayOfMonth<=22)) {
	alert(`Your zodiac sign is ${zodiacSigns[5]}`);
} else if ((month===9 && dayOfMonth>=23) || (month===10 && dayOfMonth<=22)) {
	alert(`Your zodiac sign is ${zodiacSigns[6]}`);
} else if ((month===10 && dayOfMonth>=23) || (month===11 && dayOfMonth<=21)) {
	alert(`Your zodiac sign is ${zodiacSigns[7]}`);
} else if ((month===11 && dayOfMonth>=22) || (month===12 && dayOfMonth<=21)) {
	alert(`Your zodiac sign is ${zodiacSigns[8]}`);
} else if ((month===12 && dayOfMonth>=22) || (month===1 && dayOfMonth<=19)) {
	alert(`Your zodiac sign is ${zodiacSigns[9]}`);
} else if ((month===1 && dayOfMonth>=20) || (month===2 && dayOfMonth<=18)) {
	alert(`Your zodiac sign is ${zodiacSigns[10]}`);
} else if ((month===2 && dayOfMonth>=19) || (month===3 && dayOfMonth<=20)) {
	alert(`Your zodiac sign is ${zodiacSigns[11]}`);
} else if ((dayOfMonth<1) || (month===2 && dayOfMonth>29)) {
	alert("The date is wrong");
} else if (longMonthes.some(isLongMonth) && (dayOfMonth>31)) {
	alert("The date is wrong");
} else if (shortMonthes.some(isShortMonth) && (dayOfMonth>30)) {
	alert("The date is wrong");
} else {
	alert("The date is wrong");
};

// Определяем возраст
let yearsAmount = nowYear - year;

if ((nowMonth < month) || ((nowMonth === month) && (nowDayOfMonth < dayOfMonth))) {
	alert(`You have ${yearsAmount - 1} years`);
} else if ((dayOfMonth<1) || (month===2 && (dayOfMonth>29))) {
	alert("The date is wrong");
} else if (longMonthes.some(isLongMonth) && (dayOfMonth>31)) {
	alert("The date is wrong");
} else if (shortMonthes.some(isShortMonth) && (dayOfMonth>30)) {
	alert("The date is wrong");
} else {
	alert(`You have ${yearsAmount} years`);
};


// если день в котором родился > чем день сейчас, тогда день в котором родился уменьшаем на 1